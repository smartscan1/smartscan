import React, {useState} from 'react'
import MyButton from '../../UI/button/MyButton';


function DeviceForm({create}) {
  
  const [IP, setIP] = useState('');
  const [title, setTitle] = useState('');
  
  const addNewDevice = (evt) =>{
    evt.preventDefault()
    const newDevice = {
      id: Date.now(),
      IP,
      title
    }
    create(newDevice)
    setIP('')
    setTitle('')
  }

  return (
    <div >
      <form className='modalform'>
        <input 
          className='input'
          value={IP}
          onChange={evt => setIP(evt.target.value)}
          type='text' 
          placeholder='Введите IP устройства'/>
        <input 
          className='input'
          value={title}
          onChange={evt => setTitle(evt.target.value)}
          type='text' 
          placeholder='Введите тип устройства'/>
        <MyButton onClick={addNewDevice}>Добавить</MyButton>
      </form>
    </div>  
  )
}

export default DeviceForm