import React, {useState} from 'react'
import DeviceList from './DeviceList';
import DeviceForm from './DeviceForm';
import MyModal from '../../UI/modal/MyModal';
import MyButton from '../../UI/button/MyButton';
import {IoMdAddCircleOutline} from 'react-icons/io'
import SaveBtn from '../../components/SaveBtn';
import DisplayDeviceItem from './DisplayDeviceItem'


export default function Devices() {
  const [modal, setModal] = useState(false)
  const [devices, setDevices] = useState([
    {id: 1, IP: '192.355', title: 'Scaner'},
    {id: 2, IP: '192.355', title: 'Scaner'},
    {id: 3, IP: '192.355', title: 'Scaner'}
  ]);
  
  const createDevice = (newDevice) =>{
    setDevices([...devices, newDevice])
    setModal(false)
  }

  const removeDevice = (device) => {
    setDevices(devices.filter(d => d.id !== device.id))
  }
  const handleSeve = () => {
    console.log('Save Button' )
  }

  return (
    <div className='wrapper dev-wrap'>
      <div className='devcontent'>
        <div className='devlist'>
          <div>Список устройств</div>
          <MyButton onClick = {() => setModal(true)}>Новое устройство <IoMdAddCircleOutline/></MyButton>
        </div>
        <MyModal active={modal} setActive={setModal}>
          <DeviceForm create={createDevice}/>
        </MyModal>
        <DeviceList devices={devices} remove={removeDevice}  />
        <DisplayDeviceItem devices={devices}/>

      </div>
      <div>
        <div>Сообщения от системы</div>
        <textarea className='input areainput ' placeholder='Log text...'/>
      </div>
      <SaveBtn onClick={handleSeve}/>
    </div>
  )
}

