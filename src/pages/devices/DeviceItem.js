import React from 'react'
import {TiDelete} from 'react-icons/ti'


function DeviceItem(props) {
 
  return (
    <div className='devitem' onClick={()=> console.log('click')}>
      <div>
        <div>{props.device.IP}</div>
        <div>{props.device.title}</div>
      </div>
      <div>
        <button 
          className='removebtn' 
          onClick={() => props.remove(props.device)}
        >
          <TiDelete/>
        </button>
      </div>
    </div>
  )
}

export default DeviceItem