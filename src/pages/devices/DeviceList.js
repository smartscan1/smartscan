import React from 'react'
import DeviceItem from './DeviceItem'

function DeviceList({devices, remove}) {
  if(!devices.length){
    return <h2>Устройств нет</h2>
  }
  return (
      <div>
        {
        devices.map(device =>
          <DeviceItem remove={remove} device={device} key={device.id} />
        )
      }
      </div>    
  )
}

export default DeviceList