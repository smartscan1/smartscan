import React, {useState} from 'react'
import InputForm from './InputForm'
import SaveBtn from '../../components/SaveBtn';

export default function Settings() {
  const [inputValues, setInputValues] = useState();

  const handleInputChange = (inputName, newValue) => {
    setInputValues((prevInputValues) => ({
      ...prevInputValues,
      [inputName]: newValue,
    }));
  };
  const handleSeve = () => {
    console.log('Completed', inputValues )
  }
    
  return (
    <div className='wrapper'>
      <InputForm 
        inputName={'API port'} 
        placeholderText={'9090'}
        onInputChange={handleInputChange}
      />
      <InputForm 
        inputName={'WEB port'}
        placeholderText={'9091'}
        onInputChange={handleInputChange}
      />
      <InputForm 
        inputName={'API server'} 
        placeholderText={'http://0.0.0.0.9090'}
        onInputChange={handleInputChange}
      />
      <SaveBtn onClick={handleSeve}/>
    </div>
  )
}
