import React, {useState} from 'react'

function InputForm({inputName, placeholderText, onInputChange}) {
  const [inputValue, setInputValue] = useState('');

  const handleInputChange = event => {
    const newValue = event.target.value 
    setInputValue(newValue)
    onInputChange(inputName, newValue)
  }
  return (
    <form className='form' onSubmit={(evt) => evt.preventDefault()}>
      <label className='label' >{inputName}</label>
      <input 
        className='input'
        type="text"
        // id={inputName}
        value={inputValue}
        onChange={handleInputChange}
        placeholder={placeholderText}
      />
    </form>
  )
}

export default InputForm