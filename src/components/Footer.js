import React from 'react'

function Footer() {
  return (

    <div className='footer'>
      <hr/>
      <div className='footer_item'>
        <div>
          <p>(c) SmartScan 2023. Все права защищены</p>
        </div>
        <div>
          <p>119071, Москва, Ленинский проспект, д22. офис 11</p>
          <p>+7 495 223 45 67</p>
          <p>info@smartscan.io</p>
        </div>
      </div>

    </div>
  )
}

export default Footer