import React from 'react'
import { NavLink } from 'react-router-dom'

function Navbar() {
  return (
    <>
      <h2 style={{padding: '25px'}}>
        SmartScan администратирование
      </h2>
      <hr/>
      <nav className='navbar'>
        <NavLink to='/settings'>Настройки</NavLink>
        <NavLink to='/'>Устройства</NavLink>
        <NavLink to='/license'>Лицензия</NavLink>
      </nav>
    </>
  )
}

export default Navbar