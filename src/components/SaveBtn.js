import React from 'react'
import MyButton from '../UI/button/MyButton';
import {RiSave2Line} from 'react-icons/ri'


function SaveBtn({onClick}) {

  return (
    <div className='savebtn'>
        <MyButton
         type='submit' 
         onClick={onClick}
        >
            Сохранить <RiSave2Line/>
        </MyButton>
    </div>

  )
}

export default SaveBtn