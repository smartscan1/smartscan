import './App.css';
import './Style.css'
import {Routes, Route} from 'react-router-dom';
import Devices from './pages/devices/Devices';
import Settings from './pages/settings/Settings';
import License from './pages/license/License';
import Navbar from './components/Navbar';
import Footer from './components/Footer';

function App() {
  return (
    <div className='App'>
      <Navbar/>
      <Routes >
        <Route path='settings' element={<Settings/>}/>
        <Route path='/' element={<Devices/>}/>
        <Route path='license' element={<License/>}/>
      </Routes>

      <Footer/>
    </div>
  );
}

export default App;
