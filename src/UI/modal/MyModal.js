import React from 'react';
import "./MyModal.css"
export default function MyModal ({children, active, setActive}) {
    return(
        <div className={active ? "myModal active" : "myModal"} onClick={() => setActive(false)} >
            <div className="myModalContent" onClick={e => e.stopPropagation()}>
                {children}
            </div>

        </div>
    )
}